from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from django.http import HttpResponseRedirect

from django.shortcuts import render
from .forms import Add_Email
from .models import Person

response = {}

def index(request):
    response = {'nama_emak' : 'Ibu Kita Kartini', 'nama_saya' : 'Hotman'}
    response['form_email'] = Add_Email
    return render(request,'story5/index.html',response)

def savemail(request):
	form = Add_Email(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['email'] = request.POST['email']
		person = Person(email=response['email'])
		person.save()
		return HttpResponseRedirect('/')
	else:
		return HttpResponseRedirect('/')
def contact(request):
	return HttpResponse ("Ini halaman contact")
