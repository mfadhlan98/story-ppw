"""story_ppw URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include, re_path
import story1.urls as story1
import story3.urls as story3
import story4.urls as story4

urlpatterns = [
    path('admin/', admin.site.urls),
    path('story4/', include('story4.urls')),
    re_path(r'^story1/', include(story1)),
    re_path(r'^story3/', include(story3)),
    re_path(r'^story4/', include(story4)),

]
