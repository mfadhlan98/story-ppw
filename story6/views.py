from django.http import HttpResponse
from django.http import HttpResponseRedirect

from django.shortcuts import render
from .forms import Add_Name
from .models import Person

response = {}

mhs_name = 'Muhammad Fadhlan'

def index(request):
    response = {'nama_emak' : 'Ibu Kita Kartini', 'nama_saya' : 'Hotman'}
    response['form_name'] = Add_Name
    return render(request,'story5/index.html',response)

def savename(request):
	form = Add_Name(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['name'] = request.POST['name']
		person = Person(name=response['name'])
		person.save()
		return HttpResponseRedirect('/')
	else:
		return HttpResponseRedirect('/')
        
def contact(request):
	return HttpResponse ("Ini halaman contact")

def test_landing_page_content_is_written(self):
    #Content cannot be null
    self.assertIsNotNone(landing_page_content)

    #Content is filled with 30 characters
    self.assertTrue(len(landing_page_content) >= 30)

def test_landing_page_is_completed(self):
    request = HttpRequest()
    response = index(request)
    html_response = response.content.decode('utf8')
    self.assertIn('Hello, this is '+ mhs_name + '.', html_response)
    self.assertIn(test_landing_page_content, html_response)