from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse

mhs_name = 'Mahasiswa Fasilkom'

def index(request):
    response = {'name' : mhs_name}
    return render(request,'index.html',response)

def fungsi(request):
    return render(request, 'Home.html')

def about(request):
    return render(request, 'About.html')