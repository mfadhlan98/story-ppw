from django import forms

class Add_Name(forms.Form):

	pos_attrs = {
        'type': 'name',
        'class': 'form-control',
        'id':'exampleInputName',
        'aria-describedby':'nameHelp',
        'placeholder':'Enter Name'
    }
	email = forms.CharField(label='nama', required=True, max_length=30, widget=forms.TextInput(attrs=pos_attrs))

class Add_Address(forms.Form):

	pos_attrs = {
        'type': 'address',
        'class': 'form-control',
        'id':'exampleInputAddress',
        'aria-describedby':'addressHelp',
        'placeholder':'Enter address'
    }
	email = forms.CharField(label='alamat', required=True, max_length=50, widget=forms.TextInput(attrs=pos_attrs))
