from django.urls import include, path
from .views import index, fungsi, about

urlpatterns = [
    path('', index, name='index'),
    path('home', fungsi, name='home'),
    path('about', about, name='about'),
]