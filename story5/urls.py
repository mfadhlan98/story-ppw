from django.urls import include, path
from .views import index, contact, savemail

urlpatterns = [
    path('project', index, name='index'),
    path('contact', contact, name='contact'),
    path('', index, name='noname'),
    path('savemail', savemail, name='savemail')
]
