from django import forms

class Add_Email(forms.Form):

	pos_attrs = {
        'type': 'email',
        'class': 'form-control',
        'id':'exampleInputEmail1',
        'aria-describedby':'emailHelp',
        'placeholder':'Enter email'
    }
	email = forms.CharField(label='email', required=True, max_length=30, widget=forms.TextInput(attrs=pos_attrs))
